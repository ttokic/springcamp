from django.conf.urls import url
from Forme.apps.landing_page import views
urlpatterns = [
    url(r'^$', views.landing_page, name='landing'),
]