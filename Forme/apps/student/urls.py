from django.conf.urls import url
from Forme.apps.student import views
urlpatterns = [
    url(r'^list/$', views.StudentListView.as_view(), name='list_students'),
    url(r'^create_student/$', views.CreateStudent.as_view(), name='create_student'),
    url(r'^edit_student/(?P<pk>([0-9]+))/$', views.UpdateStudent.as_view(), name='edit_student'),
]