from django.core.urlresolvers import  reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView

from Forme.apps.student.models import Student
from Forme.apps.student.forms import CreateStudentForm


class StudentListView(ListView):
    model = Student
    template_name = 'students.html'

    def get_context_data(self, **kwargs):
        context_data = super(StudentListView, self).get_context_data(**kwargs)
        context_data['page_title'] = 'Student List Page'
        return context_data


class CreateStudent(CreateView):
    model = Student
    template_name = 'create_student.html'
    form_class = CreateStudentForm
    success_url = reverse_lazy('student:list_students')

    def get_context_data(self, **kwargs):
        context_data = super(CreateStudent, self).get_context_data()
        context_data['page_title'] = 'Create Student'
        return context_data


class UpdateStudent(UpdateView):
    model = Student
    template_name = 'create_student.html'
    form_class = CreateStudentForm
    success_url = reverse_lazy('student:list_students')

    def get_context_data(self, **kwargs):
        context_data = super(UpdateStudent, self).get_context_data()
        context_data['page_title'] = 'Edit Student'
        return context_data
